![](docs/resources/img/OpenD6_logo.png)
[![GNU Affero General Public License v3.0 or later][AGPL-Badge]][AGPL-3.0-or-later]
[![Open Game License][OGL-Badge]][OGL]
[![Python Package Index][PyPI-Badge]][PyPI]

# PyOpenD6

_[RESTful][REST] Python API based on [OpenD6][OpenD6]._

## License

### Source Code

[GNU Affero General Public License v3.0 or later][AGPL-3.0-or-later].

### Documentation

[Creative Commons Attribution-ShareAlike 4.0 International License][CC-BY-SA-4.0].

### Content

[Open Game License][OGL]

## Installing

### From Source Code

```sh
git clone https://gitlab.com/hexgear/pyopendsix.git
cd pyopendsix
```

#### Virtualenv

```sh
virtualenv venv
source venv/bin/activate
python setup.py install
```

#### Docker-compose

```sh
docker-compose build
```

### PyPI

```sh
pip install pyopendsix
```

## Running

### Flask

After installing on the local system:

```sh
python -m pyopendsix
```

### Gunicorn

Running directly from the git repository:

```sh
gunicorn -b 0.0.0.0:6000 wsgi:api
```

### Docker

#### Docker Hub

Images can be run directly from Docker Hub:

```sh
docker run hexgear/pyopendsix
```

#### Docker-compose

Running directly from the git repository:

```sh
docker-compose up
```

### Heroku

Running on the free tier:

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/hexgear/pyopendsix)

[CC-BY-SA-4.0]: http://creativecommons.org/licenses/by-sa/4.0
[AGPL-3.0-or-later]: COPYING
[AGPL-Badge]: https://img.shields.io/badge/license-AGPL--3.0--or--later-blue.svg?style=flat-square
[OpenD6]: https://ogc.rpglibrary.org/index.php?title=OpenD6
[OGL]: http://www.opengamingfoundation.org/ogl.html
[OGL-Badge]: https://img.shields.io/badge/license-OGL-green.svg?style=flat-square
[PyPI]: https://pypi.python.org/pypi/pyopendsix
[PyPI-Badge]: https://img.shields.io/pypi/v/pyopendsix.svg?style=flat-square
[REST]: https://en.wikipedia.org/wiki/Representational_state_transfer
