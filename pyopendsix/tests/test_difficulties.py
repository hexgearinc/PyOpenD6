#!/usr/env/python
# coding: utf-8

"""
Test Standard Difficulties
"""

import unittest
from pyopendsix import difficulties

class TestDifficulties(unittest.TestCase):

    def test_non_numeric_difficulty(self):
        self.fail("Not yet implemented.")

    def test_negative_difficulty(self):
        self.fail("Not yet implemented.")

    def test_automatic_difficulty(self):
        automatic = difficulties.get_difficulty(0)
        self.assertEqual(automatic, 'automatic')

    def test_very_easy_difficulty(self):
        for action in range(1,5):
            very_easy = difficulties.get_difficulty(action)
            self.assertEqual(very_easy, 'very_easy')

    def test_easy_difficulty(self):
        for action in range(6,10):
            easy = difficulties.get_difficulty(action)
            self.assertEqual(easy, 'easy')

    def test_moderate_difficulty(self):
        for action in range(11,15):
            moderate = difficulties.get_difficulty(action)
            self.assertEqual(moderate, 'moderate')

    def test_difficult_difficulty(self):
        for action in range(16,20):
            difficult = difficulties.get_difficulty(action)
            self.assertEqual(difficult, 'difficult')

    def test_very_difficult_difficulty(self):
        for action in range(21,25):
            very_difficult = difficulties.get_difficulty(action)
            self.assertEqual(very_difficult, 'very_difficult')

    def test_heroic_difficulty(self):
        for action in range(26,30):
            heroic = difficulties.get_difficulty(action)
            self.assertEqual(heroic, 'heroic')

    def test_legendary_difficulty(self):
        for action in range(31,40):
            legendary = difficulties.get_difficulty(action)
            self.assertEqual(legendary, 'legendary')

if __name__ == '__main__':
    unittest.main()
