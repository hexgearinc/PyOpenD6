#!/usr/env/python
# coding: utf-8

"""
Wound Levels
"""

def get_effect(damage, wounded=False):
    """
    >>> wound = get_wound_effect(damage)
    """
    if damage > 0 <= 3:
        effect = "stunned"
    elif damage >= 4 <= 8:
        if not wounded:
            effect = "wounded"
        else:
            effect = "severely_wounded"
    elif damage >= 9 <= 12:
        effect = "incapacitated"
    elif damage >= 13 <= 15:
        effect = "mortally_wounded"
    elif damage >= 16:
        effect = "dead"
    else:
        effect = "bruised"

    return effect
